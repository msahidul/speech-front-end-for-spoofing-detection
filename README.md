# Speech Front-end for Spoofing Detection

This repository contains the audio features evaluated in the following paper:
M. Sahidullah, T. Kinnunen, C. Hanilçi, ”A comparison of features for synthetic speech detection”, Proc. Interspeech 2015, pp. 2087--2091, Dresden, Germany, September 2015